### Usage

This is a normal sbt project. You can compile code with `sbt compile`, run it
with `sbt run`, and `sbt console` will start a Scala 3 REPL.

#### To run from the command line of your operating system:
`$ sbt "run HOT 1, 2, 3"`

Note the string includes the sbt command 'run' as well as the arguments passed
to the program.

#### To run from within the sbt console:
`sbt> run HOT 1, 2, 3`
