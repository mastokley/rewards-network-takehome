package codingChallenge

import codingChallenge.resource

@main def getDressed(temperature: String, commands: String*): Unit =
  val out = resource.getDressed(temperature, commands.mkString).get
  println(out)
