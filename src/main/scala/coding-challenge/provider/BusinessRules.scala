package codingChallenge.provider

import codingChallenge.domain._
import codingChallenge.domain.ArticleOfClothing._
import codingChallenge.domain.BodyPart._
import codingChallenge.domain.Location._
import codingChallenge.domain.State._
import codingChallenge.domain.Temperature._

val takeOffPJsFirst: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(_)) if isWearing(state, PJs) => false

val distinctArticlesOfClothing: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(article)) if isWearing(state, article) => false

val noSocksInHotWeather: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(Socks)) if state.temperature == Hot => false

val noJacketInHotWeather: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(Jacket)) if state.temperature == Hot => false

val socksBeforeFootwear: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(Boots)) if !isWearing(state, Socks) => false

val pantsBeforeFootwear: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(Boots))   if !isClothed(state, Legs) => false
  case (state, PutOn(Sandals)) if !isClothed(state, Legs) => false

val shirtBeforeHeadwear: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(Sunglasses)) if !isWearing(state, Shirt) => false
  case (state, PutOn(Hat))        if !isWearing(state, Shirt) => false

val shirtBeforeJacket: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, PutOn(Jacket)) if !isWearing(state, Shirt) => false

val leaveHouseFullyDressed: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (state, Move(OutsideOfTheHouse)) if !isFullyDressed(state) => false

// NOTE: this absolutely must come last!
// could that be expressed with types?
val default: PartialFunction[(State, ExecutableCommand), Boolean] =
  case (_, _) => true

val isValid = Seq(takeOffPJsFirst,
                  distinctArticlesOfClothing,
                  noSocksInHotWeather,
                  noJacketInHotWeather,
                  socksBeforeFootwear,
                  pantsBeforeFootwear,
                  shirtBeforeHeadwear,
                  shirtBeforeJacket,
                  leaveHouseFullyDressed,
                  default).reduce(_ orElse _ )
