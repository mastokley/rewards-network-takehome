package codingChallenge.provider

import codingChallenge.domain.ArticleOfClothing._
import codingChallenge.domain.BodyPart._
import codingChallenge.domain.Location._
import codingChallenge.domain.State.{putOn, takeOff, move}
import codingChallenge.domain.Temperature._
import codingChallenge.domain._

def toPutOn(temperature: Temperature, dress: Dress): PutOn =
  dress match
    case Dress(Feet) if temperature == Hot  => PutOn(Sandals)
    case Dress(Feet) if temperature == Cold => PutOn(Boots)
    case Dress(Head) if temperature == Hot  => PutOn(Sunglasses)
    case Dress(Head) if temperature == Cold => PutOn(Hat)
    case Dress(Legs) if temperature == Hot  => PutOn(Shorts)
    case Dress(Legs) if temperature == Cold => PutOn(Pants)
    case _                                  =>
      throw new Exception(s"unknown dress command: $dress")

def toExecutable(temperature: Temperature,
                 commands: Seq[Command]): Seq[ExecutableCommand] =
  commands.map {
    case dress: Dress     => toPutOn(temperature, dress)
    case putOn: PutOn     => putOn
    case takeOff: TakeOff => takeOff
    case move: Move       => move
  }

val putOnFootwear: PartialFunction[(State, ExecutableCommand), State] =
  case (state, PutOn(Sandals)) if state.temperature == Hot =>
    putOn(state, Sandals, "sandals")
  case (state, PutOn(Boots)) if state.temperature == Cold =>
    putOn(state, Boots, "boots")

val putOnHeadwear: PartialFunction[(State, ExecutableCommand), State] =
  case (state, PutOn(Sunglasses)) if state.temperature == Hot =>
    putOn(state, Sunglasses, "sunglasses")
  case (state, PutOn(Hat)) if state.temperature == Cold =>
    putOn(state, Hat, "hat")

val putOnLegwear: PartialFunction[(State, ExecutableCommand), State] =
  case (state, PutOn(Shorts)) if state.temperature == Hot =>
    putOn(state, Shorts, "shorts")
  case (state, PutOn(Pants)) if state.temperature == Cold =>
    putOn(state, Pants, "pants")

val putOnSocks: PartialFunction[(State, ExecutableCommand), State] =
  case (state, PutOn(Socks)) if state.temperature == Cold =>
    putOn(state, Socks, "socks")

val putOnShirt: PartialFunction[(State, ExecutableCommand), State] =
  case (state, PutOn(Shirt)) => putOn(state, Shirt, "shirt")

val putOnJacket: PartialFunction[(State, ExecutableCommand), State] =
  case (state, PutOn(Jacket)) if state.temperature == Cold =>
    putOn(state, Jacket, "jacket")

val leaveHouse: PartialFunction[(State, ExecutableCommand), State] =
  case (state, Move(OutsideOfTheHouse)) =>
    move(state, OutsideOfTheHouse, "leaving house")

val takeOffPajamas: PartialFunction[(State, ExecutableCommand), State] =
  case (state, TakeOff(PJs)) => takeOff(state, PJs, "Removing PJs")

val transitionState = Seq(putOnFootwear,
                          putOnHeadwear,
                          putOnSocks,
                          putOnShirt,
                          putOnJacket,
                          putOnLegwear,
                          leaveHouse,
                          takeOffPajamas).reduce(_ orElse _)
