package codingChallenge.provider

import codingChallenge.domain._
import scala.annotation.tailrec

@tailrec
private def getDressed(state: State, commands: List[ExecutableCommand]): State =
  commands match {
    case Nil                                      => state
    case c :: cs if (isValid((state, c)))         =>
      val nextState = transitionState((state, c))
      getDressed(nextState, cs)
    case c :: cs                                  =>
      state.copy(eventLog = state.eventLog :+ "fail")
  }

def getDressed(temperature: Temperature, commands: Seq[Command]): State =
  val initialState = State(Location.InsideOfTheHouse,
                           ClothingState(Set(ArticleOfClothing.PJs)),
                           temperature)
  val executableCommands = toExecutable(temperature, commands)
  getDressed(initialState, executableCommands.toList)
