package codingChallenge.resource

import codingChallenge.provider
import scala.util.{Success, Failure, Try}

def getDressed(temperature: String, commands: String): Try[String] =
  for {
    (t, cs) <- parseInput(temperature, commands)
    finalState = provider.getDressed(t, cs)
    outStr = toOutStr(finalState)
  } yield(outStr)
