package codingChallenge.resource

import codingChallenge.domain.State

def toOutStr(finalState: State): String =
  finalState.eventLog.mkString(", ")

