package codingChallenge.resource

import cats._
import cats.data._
import cats.implicits._
import codingChallenge.domain.ArticleOfClothing._
import codingChallenge.domain.BodyPart._
import codingChallenge.domain.Location._
import codingChallenge.domain.Temperature._
import codingChallenge.domain._
import codingChallenge.util.toTry
import scala.util.{Try, Success, Failure}

private def parseTemperature(temperature: String): Try[Temperature] =
  temperature match {
    case "HOT"  => Success(Hot)
    case "COLD" => Success(Cold)
    case _      => Failure(new Throwable(s"unknown temperature $temperature"))
  }

private def parseCommands(commands: String): Try[Seq[Command]] =
  commands
    .split(",")
    .map(_.trim)
    .map(_.toInt)
    .map(toDomainCommand.get)
    .map(toTry(s"unknown command; expected one of ${toDomainCommand.keys}"))
    .toSeq
    .sequence

private val toDomainCommand: Map[Int, Command] =
  Map(1 -> Dress(Feet),
      2 -> Dress(Head),
      3 -> PutOn(Socks),
      4 -> PutOn(Shirt),
      5 -> PutOn(Jacket),
      6 -> Dress(Legs),
      7 -> Move(OutsideOfTheHouse),
      8 -> TakeOff(PJs))

def parseInput(temperature: String,
               commands: String): Try[(Temperature, Seq[Command])] =
  for {
    t  <- parseTemperature(temperature)
    cs <- parseCommands(commands)
  } yield(t, cs)
