package codingChallenge.domain

// NOTE probably don't need to model the rest of the parts of the body
enum BodyPart:
    case Head, Feet, Legs

enum Temperature:
    case Hot, Cold

enum Location:
    case InsideOfTheHouse, OutsideOfTheHouse

// TODO Sandals and Boots are both footwear
// TODO Shorts and Pants are both legwear
// these could be expressed by types
enum ArticleOfClothing:
    case Sandals, Boots, Sunglasses, Hat, Socks, Shirt, Jacket, Shorts, Pants, PJs

case class ClothingState(clothingWorn: Set[ArticleOfClothing])

case class State(location: Location,
                 clothingState: ClothingState,
                 temperature: Temperature,
                 eventLog: Seq[String] = Seq.empty)

object State {

  import ArticleOfClothing._
  import BodyPart._
  import Temperature._

  def putOn(state: State, articleOfClothing: ArticleOfClothing, event: String) =
    val clothingState = state.clothingState.copy(
      state.clothingState.clothingWorn + articleOfClothing)
    state.copy(clothingState = clothingState,
               eventLog = state.eventLog :+ event)

  def move(state: State, destination: Location, event: String) =
    state.copy(location = destination, eventLog = state.eventLog :+ event)

  def takeOff(state: State, articleOfClothing: ArticleOfClothing, event: String) =
    val clothingState = state.clothingState.copy(
      state.clothingState.clothingWorn - articleOfClothing)
    state.copy(clothingState = clothingState,
               eventLog = state.eventLog :+ event)

  def isWearing(state: State, articleOfClothing: ArticleOfClothing): Boolean =
    state.clothingState.clothingWorn.contains(articleOfClothing)

  def isWearing(state: State, articlesOfClothing: Set[ArticleOfClothing]): Boolean =
    articlesOfClothing subsetOf state.clothingState.clothingWorn

  // this feels clumsy to me - should be a way to generalize this
  // perhaps by mapping articles of clothing to parts of the body
  // then you could calculate whether any given part of the body were clothed
  def isClothed(state: State, bodyPart: BodyPart): Boolean =
    bodyPart match
      case Head =>
        state.clothingState.clothingWorn.intersect(Set(Sunglasses, Hat)).nonEmpty
      case Legs =>
        state.clothingState.clothingWorn.intersect(Set(Pants, Shorts)).nonEmpty
      case Feet =>
        state.clothingState.clothingWorn.intersect(Set(Sandals, Boots)).nonEmpty

  // this probably belongs in `BusinessRules` because it's so opinionated
  def isFullyDressed(state: State): Boolean =
    state.temperature match {
      case Hot  => isWearing(state, Set(Sandals, Sunglasses, Shirt, Shorts))
      case Cold => isWearing(state, Set(Boots, Hat, Shirt, Pants, Jacket, Socks))
    }

}
