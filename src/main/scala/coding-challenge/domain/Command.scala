package codingChallenge.domain

trait Command

trait ExecutableCommand

case class Move(destination: Location)
    extends Command with ExecutableCommand

case class PutOn(articleOfClothing: ArticleOfClothing)
    extends Command with ExecutableCommand

case class TakeOff(articleOfClothing: ArticleOfClothing)
    extends Command with ExecutableCommand

case class Dress(bodyPart: BodyPart) extends Command
