package codingChallenge.util

import scala.util.{Try, Success, Failure}

def toTry[T](message: String)(option: Option[T]): Try[T] =
  option match {
    case Some(t) => Success(t)
    case None    => Failure(new Exception(message))
  }
