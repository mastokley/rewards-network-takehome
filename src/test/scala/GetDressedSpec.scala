import codingChallenge.resource.{parseInput, getDressed}
import codingChallenge.provider._
import scala.util.{Success, Try}
import codingChallenge.domain._

class GetDressedSuite extends munit.FunSuite {

  test("dressing for a hot day") {
    val temp = "HOT"
    val commands = "8, 6, 4, 2, 1, 7"
    val expected = "Removing PJs, shorts, shirt, sunglasses, sandals, leaving house"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("dressing for a cold day") {
    val temp = "COLD"
    val commands = "8, 6, 3, 4, 2, 5, 1, 7"
    val expected = "Removing PJs, pants, socks, shirt, hat, jacket, boots, leaving house"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("no duplicate articles") {
    val temp = "HOT"
    val commands = "8, 6, 6"
    val expected = "Removing PJs, shorts, fail"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("no socks on a hot day") {
    val temp = "HOT"
    val commands = "8, 6, 3"
    val expected = "Removing PJs, shorts, fail"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("leaving the house without dressing appropriately for the weather") {
    val temp = "COLD"
    val commands = "8, 6, 3, 4, 2, 5, 7"
    val expected = "Removing PJs, pants, socks, shirt, hat, jacket, fail"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("wearing PJs underneath street clothes") {
    val temp = "COLD"
    val commands = "6"
    val expected = "fail"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("require pants before footwear") {
    val temp = "COLD"
    val commands = "8, 3, 1"
    val expected = "Removing PJs, socks, fail"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("require socks before boots") {
    val temp = "COLD"
    val commands = "8, 6, 1"
    val expected = "Removing PJs, pants, fail"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("don't require socks before sandals") {
    val temp = "HOT"
    val commands = "8, 6, 1"
    val expected = "Removing PJs, shorts, sandals"
    val obtained = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("require shirt prior to sunglasses") {
    val temp = "HOT"
    val commands = "8, 2"
    val expected = "Removing PJs, fail"
    val obtained  = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("require shirt prior to hat") {
    val temp = "COLD"
    val commands = "8, 2"
    val expected = "Removing PJs, fail"
    val obtained  = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("require shirt prior to jacket") {
    val temp = "COLD"
    val commands = "8, 5"
    val expected = "Removing PJs, fail"
    val obtained  = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("dress fully before leaving house: HOT") {
    val temp = "HOT"
    val commands = "8, 4, 7"
    val expected = "Removing PJs, shirt, fail"
    val obtained  = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

  test("dress fully before leaving house: COLD") {
    val temp = "COLD"
    val commands = "8, 4, 7"
    val expected = "Removing PJs, shirt, fail"
    val obtained  = getDressed(temp, commands).get
    assertEquals(obtained, expected)
  }

}

class ParseInputSuite extends munit.FunSuite {
  test("happy path") {
    val temp = "COLD"
    val commands = "8, 6"
    val expected: Try[(Temperature, Seq[Command])] =
      Success((Temperature.Cold, Seq(TakeOff(ArticleOfClothing.PJs),
                                     Dress(BodyPart.Legs))))
    val obtained = parseInput(temp, commands)
    assertEquals(obtained, expected)
  }
}

class TransitionStateSuite extends munit.FunSuite {
  test("putting on footwear") {
    val state = State(location = Location.InsideOfTheHouse,
                      clothingState = ClothingState(Set.empty),
                      temperature = Temperature.Hot)
    val command = Dress(BodyPart.Feet)
    val executableCommand = toPutOn(Temperature.Hot, command)
    val expected = State(location = Location.InsideOfTheHouse,
                         clothingState = ClothingState(
                           Set(ArticleOfClothing.Sandals)),
                         temperature = Temperature.Hot,
                         eventLog = Seq("sandals"))
    val obtained = putOnFootwear((state, executableCommand))
    assertEquals(obtained, expected)
  }
}
