val scala3Version = "3.1.1"
val catsVersion = "2.7.0"
val munitVersion = "0.7.29"

lazy val root = project
  .in(file("."))
  .settings(
    name := "scala-coding-challenge",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies += "org.scalameta" %% "munit" % munitVersion % Test,
    libraryDependencies += "org.typelevel" %% "cats-core" % catsVersion
  )
